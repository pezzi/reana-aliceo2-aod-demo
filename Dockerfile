# ALICE O2 Physics Docker container 
FROM ubuntu:20.04
WORKDIR /alice/
ENV DEBIAN_FRONTEND=noninteractive
RUN  apt-get update -y && \
         apt-get install -y software-properties-common && \
         rm -rf /var/lib/apt/lists/* && \
         apt-get -y autoremove && \
         apt-get -y clean

RUN add-apt-repository ppa:alisw/ppa &&     apt install -y python3-alibuild && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get -y autoremove && \
    apt-get -y clean

RUN  apt-get update -y && \
         apt-get install -y \
         curl \
         time \
         libcurl4-gnutls-dev \
         build-essential \
         gfortran \
         libmysqlclient-dev \
         xorg-dev \
         libglu1-mesa-dev \
         libfftw3-dev \
         libxml2-dev \
         git \
         unzip \
         autoconf \
         automake \
         autopoint \
         texinfo \
         gettext \
         libtool \
         libtool-bin \
         pkg-config \
         bison \
         flex \
         libperl-dev \
         libbz2-dev \
         swig \
         liblzma-dev \
         libnanomsg-dev \
         rsync \
         lsb-release \
         environment-modules \
         libglfw3-dev \
         libtbb-dev \
         python3-dev \
         python3-venv \
         libncurses-dev \
         software-properties-common && \
         rm -rf /var/lib/apt/lists/* && \
         apt-get -y autoremove && \
         apt-get -y clean
ENV ALIBUILD_WORK_DIR /alice/sw
RUN mkdir -p /alice && \
    cd /alice && \
    aliBuild init O2Physics@master --defaults o2 && \
    aliBuild build O2Physics --defaults o2 --debug 
#    rm -rf /alice/sw/TARS && \
#    rm -rf /alice/sw/MIRROR && \
#    rm -rf /alice/sw/SOURCES && \
RUN eval "`alienv shell-helper`"
ENV DEBIAN_FRONTEND=""
COPY entrypoint.sh /usr/local/bin/entrypoint.sh
COPY wrapper /usr/local/bin/wrapper
COPY O2Env /usr/local/O2Env
RUN chmod +x /usr/local/bin/entrypoint.sh && \
    chmod +x /usr/local/bin/wrapper
# Container will be started with the correct environment loaded
# ENTRYPOINT /usr/local/bin/entrypoint.sh
CMD [ "/bin/bash" ]

